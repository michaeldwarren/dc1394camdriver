# Confirm that Point Grey FlyCapture SDK is installed
#
# This module defines
# DC1394_FOUND, If false, don't try to use flycapture sdk modules

#include( ${CMAKE_ROOT}/Modules/CheckIncludeFileCXX.cmake )

FIND_PATH( DC1394_PATH dc1394.h
# installation selected by user
$ENV{DC1394_HOME}/include
# system placed in /usr/include
/usr/include/dc1394
# system placed in /usr/local/include
/usr/local/include/dc1394
)

FIND_LIBRARY(DC1394_LIBRARY dc1394
# installation selected by user
$ENV{DC1394_HOME}/lib
# system placed in /usr/lib
/usr/lib
# system placed in /usr/local/lib
/usr/local/lib
)

if( DC1394_PATH AND DC1394_LIBRARY )
    message( STATUS "Looking for DC1394 v2 - found")
    SET ( DC1394_FOUND 1 )
else( DC1394_PATH AND DC1394_LIBRARY  )
    message( STATUS "Looking for DC1394 v2 - not found")
    SET ( DC1394_FOUND 0 )
endif( DC1394_PATH AND DC1394_LIBRARY )
