# Confirm that Point Grey FlyCapture SDK is installed
#
# This module defines
# VXL_FOUND, If false, don't try to use flycapture sdk modules

#include( ${CMAKE_ROOT}/Modules/CheckIncludeFileCXX.cmake )

FIND_PATH( VXL_PATH core
# installation selected by user
$ENV{VXL_HOME}/include
# system placed in /usr/include
/usr/include/vxl/
# system placed in /usr/local/include
/usr/local/include/vxl/
)

if( VXL_PATH )
    message( STATUS "Looking for vxl - found")
    MESSAGE( STATUS "Vxl include path: " ${VXL_PATH})
    SET ( VXL_FOUND 1 )
    INCLUDE_DIRECTORIES( ${VXL_PATH}/core ${VXL_PATH}/core/vil  ${VXL_PATH}/vcl)
else( VXL_PATH )
    message( STATUS "Looking for vxl  - not found")
    SET ( VXL_FOUND 0 )
endif( VXL_PATH )
