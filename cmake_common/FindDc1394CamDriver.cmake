# Confirm that DC1394cam_driver is installed
#
# This module defines
# DC1394CAMDRIVER_FOUND, If false, don't try to use flycapture sdk modules

#include( ${CMAKE_ROOT}/Modules/CheckIncludeFileCXX.cmake )

FIND_PATH( DC1394CAMDRIVER_PATH dc1394cam_driver.h
# installation selected by user
$ENV{DC1394CAMDRIVER_HOME}/include
# system placed in /usr/include
/usr/include/dc1394cam_driver
# system placed in /usr/local/include
/usr/local/include/dc1394cam_driver
)

FIND_LIBRARY(DC1394CAMDRIVER_LIBRARY dc1394cam_driver
# installation selected by user
$ENV{DC1394CAMDRIVER_HOME}/lib
# system placed in /usr/lib
/usr/lib
# system placed in /usr/local/lib
/usr/local/lib
)

if( DC1394CAMDRIVER_PATH AND DC1394CAMDRIVER_LIBRARY )
    message( STATUS "Looking for DC1394cam_driver - found")
    SET ( DC1394CAMDRIVER_FOUND 1 )
else( DC1394CAMDRIVER_PATH AND DC1394CAMDRIVER_LIBRARY  )
    message( STATUS "Looking for DC1394cam_driver - not found")
    SET ( DC1394CAMDRIVER_FOUND 0 )
endif( DC1394CAMDRIVER_PATH AND DC1394CAMDRIVER_LIBRARY )
