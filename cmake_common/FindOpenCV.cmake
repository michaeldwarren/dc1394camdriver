# Locate OpenCv-0.9.9 install directory

# This module defines
# OPENCV_HOME where to find include, lib, bin, etc.
# OPENCV_FOUND, If false, don't try to use Ice.

# Find all the opencv stuff with pkg-config

FIND_PATH( OPENCV_PATH cv.h
# installation selected by user
$ENV{OPENCV_HOME}/include
# system placed in /usr/local/include
/usr/local/include/opencv
# system placed in /usr/include
/usr/include/opencv
)

if( OPENCV_PATH )
    MESSAGE( STATUS "Looking for OpenCV - found")
    MESSAGE( STATUS "OpenCV include path: "${OPENCV_PATH} )
    INCLUDE_DIRECTORIES( ${OPENCV_PATH} )
    SET ( OPENCV_FOUND 1 )
else( OPENCV_PATH )
    message( STATUS "Looking for OpenCV  - not found" )
    SET ( OPENCV_FOUND 0 )
endif( OPENCV_PATH )
