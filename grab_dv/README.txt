***** grab_dv README *****

Grabs synchronised video from multiple firewire cameras and outputs in a number of formats.

Usage:
    grab_dv v -n [NUM_FRAMES]
    -v: outputs the video streams to screen

    -n [int]: specifies the number of cameras to connect to.

    -cleanup: indicates 'cleanup' on the firewire bus. 
        Raise this flag to try and fix firewire camera 
        issues if a previous program did not shut down cleanly

    -in_format: colour, grey, bayer etc. e.g. RGB, GREY, YUV

    -output_images: this flag is necessary if the program is to output images

    -out_format [string]: 'jpg', 'bmp', 'ppm', 'pgm' etc.

    -output_video: this flag is necessary if the program is to output video (Default .avi)

    -out [string]: Output directory for the images/video

    -output_on_key: output a frame on pressing the 'c' key. (Only to images)

    -out_codec [4 letter string]: output codec for AVI (not yet used)

    -height [int] Image height. Default 480

    -width [int] Image width. Default 640

    -f [double] Camera framerate. Default 7.5

    -num_bufs [int]: Number of dc1394 driver buffers. Default 16.

    -guid_string [string]: specify which GUID's to associate with which camera. 
        Has the form: "0:49712223530833906,1:49712223530833870". 
        Camera 0 connects to GUID 49712223530833906, 1 to GUID 49712223530833870 etc.
    
    -iso_speed [int]: Desired speed of firewire bus
    
    -suppress: Suppress visualisation to save CPU cycles

    -window_width [int]: width of GUI window

    -window_height [int]: height of GUI window

Pres ESC or close the window at any time to save the video streams to disk, close the cameras and 
quit the program.

Example Uses:

./grab_dv -n 2 -window_width 360 -window_height 270 -in_format RGB -iso_speed 800 -output_images -output_on_key -width 1280 -height 960 -f 7.5 -num_bufs 8 -guid_string 0:49712223530833906,1:49712223530833870


Things to know:

    This program has been tested in Ubuntu 9.04 and 9.10 using the latest tested 
    snapshot of OpenCV as at 1/6/09. It is confirmed not to work with 
    OpenCV 1.1 due to video writing issues.


