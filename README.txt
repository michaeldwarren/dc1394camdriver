***** dc1394cam_driver README *****

A driver library that uses libdc1394-22 to grab images from firewire cameras without any 
dependancies on external libraries. Currently the driver is a little messy and still dependant on 
OpenCV, but hopefully this will change soon.

There are two classes: a class for a single camera, and a class for multiple cameras, so that
the driver can perform tasks that are multicamera aware, such as buffer synchronisation.

Any cameras (and their output images) that are accessed within the multicamera class are guaranteed to be synchronised

You need to compile the driver with 'cmake .', 'make', and then do a 'sudo make install' before you can use the
driver with your software.

Take a look at grab_dv for code examples. The main driver interface use is shown below.

Usage Examples:

    For a single camera:

        #include <dc1394cam_driver.h>

        // Create camera capture
        CamCapture capture;

        // Create vector container for images
        IplImage* image;

        //edit camera parameters
	    // If you want to connect to a specific camera, set it's guid
	    // capture.guid = 0;
        // Firewire 400 or 800
        capture.SetIsoSpeed(800);
        // Frames per second: 1.875, 3.75, 7.5, 15, 30, 60
        capture.setFrameRate("15"); // Input is a string
        // Size of dc1394 ring buffer
        capture.setDMABufSize(16);
        // Captured frame width
        capture.setWidth(640);
        // Captured frame height
        capture.setHeight(480);
        // Length of shutter time on camera. (unitless value)
        capture.setShutter(30);
        // Attempt to clean up bus after unclean process exit
        capture.setCleanup(true);
        // Suppress frame drop warnings getting printed to screen
        capture.setSuppressWarnings(false);
        if (isColor)
            capture.setColorMode("RGB"); // Get color images
        else
            capture.setColorMode("GREY"); // Get grayscale images

        // Set the camera up on the firewire bus
        capture.open();

        while(1) {
            int response = capture.grabFrame(); // Tell the driver to grab the frame from the buffer
            image = capture.retrieveFrame();  // retrieve the captured frame
        }


    For multiple cameras (including only one camera):

        #include <dc1394cam_driver.h>

        int number_of_cams = 2;

        // Create camera captures
        multiCamCapture captures(number_of_cams);

        // Create vector container for images
        vector<IplImage*> images(number_of_cams);

        /*** edit camera parameters ***/

	    // Group setup parameters
            captures.setWidthAllCams(640); // Captured frame width
            captures.setHeightAllCams(480); // Captured frame height
            //captures.setShutterAllCams(30); // Length of shutter time on camera. (unitless value)
            captures.setColorModeAllCams("RGB"); // Get RGB color images

	    // Individually setup parameters
            for (int i = 0; i < captures.size(); i++) {
                captures.setWidth(i, 640); // Captured frame width
                captures.setHeight(i, 480); // Captured frame height
                //captures.setShutter(i, 30); // Length of shutter time on camera. (unitless value)
           	    captures.setColorMode(i, "RGB"); // Get RGB color images
	    }

	    // Common setup parameters
        captures.setIsoSpeed(800); // Firewire 400 or 800
        captures.setFrameRate("15"); // Input is a string. Allowed frames per second: 1.875, 3.75, 7.5, 15, 30, 60
        captures.setDMABufSize(16); // Size of dc1394 ring buffer
        captures.setCleanup(false); // Attempt to clean up bus after unclean process exit
        captures.setSuppressWarnings(false); // Suppress frame drop warnings getting printed to screen
        // captures.SetGUID(0, 0); // If you want to connect to a specific camera, set it's guid

        // Set the cameras up on the firewire bus
        captures.open();

        while(1) {
            images = captures.queryFrame();  // Tell the driver to grab the frames from the buffers, and pass back the captured frames
        }
